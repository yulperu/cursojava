package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Persona;
import com.mitocode.model.Rol;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class IndexBean implements Serializable {

	private Usuario us;
	@Inject
	private IUsuarioService service;
	private	List<Usuario> listado;
	private String tipoDialog;
	
	@PostConstruct
	public void init() {
		this.us = new Usuario();
	}

	public String login() {
		String redireccion = "";
		try {
			Usuario usuario = service.login(us);
			if(usuario != null && usuario.getEstado().equalsIgnoreCase("A")  && usuario.getId()>0) {
				//Almacenar en la sesion de JSF y proseguir con la navegacion
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
				redireccion = "/protegido/roles?faces-redirect=true";
			}else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso",  "Credenciales incorrectas"));
		}

		return redireccion;

	}
	
	public String listar() {
		String redireccion = "";

		try {
				listado= service.listar(us);
			
		} catch (Exception e) {
			
		}

		return redireccion;

	}
	
	public String  verificar() {
		String redireccion = "";
		try {
			Usuario usuario = service.login(us);
			if(usuario != null && usuario.getEstado().equalsIgnoreCase("A")  && usuario.getId()>0) {
				//Almacenar en la sesion de JSF y proseguir con la navegacion
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales correctas"));
			}else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", e.getMessage()));
		}
		return redireccion;

		
	}
	
	@Transactional
	public String ActualizarPass() {
		String redireccion = "";
		try {			
			String clave = this.us.getContrasena1();
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.us.setContrasena(claveHash);
			this.service.modificar(us);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Se modifico contraseņa"));
			this.tipoDialog = "S";
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return redireccion;
	}

	
	public void mostrarData(Usuario p) {
		this.us = p;
		this.tipoDialog = "Modificando Usuario: " +  p.getUsuario();
		
	}


	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}

	public List<Usuario> getListado() {
		return listado;
	}

	public void setListado(List<Usuario> listado) {
		this.listado = listado;
	}

	public String getTipoDialog() {
		return tipoDialog;
	}

	public void setTipoDialog(String tipoDialog) {
		this.tipoDialog = tipoDialog;
	}

}
