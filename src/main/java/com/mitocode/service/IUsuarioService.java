package com.mitocode.service;

import java.util.List;
import com.mitocode.model.Persona;
import com.mitocode.model.PublicadorSeguidor;
import com.mitocode.model.Rol;
import com.mitocode.model.Usuario;

public interface IUsuarioService extends IService<Usuario> {
	Usuario login(Usuario us);
	List<Usuario> listar(Usuario us);
}
